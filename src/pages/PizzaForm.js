import Router from '../Router.js';
import Page from './Page.js';
import PizzaList from './PizzaList.js';

export default class AddPizzaPage extends Page {
	ingredientsHtml = '<select name="base">';
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<label>
					Base :
				</label>
				${this.ingredientsHtml}
				<label>
					Image :<br/>
					<input type="text" name="image" placeholder="https://source.unsplash.com/xxxxxxx/600x600">
					<small>Vous pouvez trouver des images de pizza sur <a href="https://unsplash.com/">https://unsplash.com/</a> puis utiliser l'URL <code>https://source.unsplash.com/xxxxxxx/600x600</code> où <code>xxxxxxx</code> est l'id de la photo choisie (celui dans la barre d'adresse)</small>
				</label>
				<label>
					Prix petit format :
					<input type="number" name="price_small" step="0.05">
				</label>
				<label>
					Prix grand format :
					<input type="number" name="price_large" step="0.05">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);

		fetch('http://localhost:8080/api/v1/ingredients')
			.then(response => response.json())
			.then(data =>
				data.map(
					el =>
						(this.ingredientsHtml += `<option value="${el.name}"> ${el.name} </option>`)
				)
			)
			.then(() => (this.ingredientsHtml += '</select>'))
			.then(() => {
				this.element.innerHTML = this.render();
				const form = this.element.querySelector('.pizzaForm');
				form.addEventListener('submit', event => {
					event.preventDefault();
					this.submit();
				});
			});
	}

	submit() {
		// D.4. La validation de la saisie

		const pizza = {
			name: this.initPizza('name'),
			image: this.initPizza('image'),
			price_small: this.initPizza('price_small'),
			price_large: this.initPizza('price_large'),
		};

		if (pizza.name === '') {
			alert('Erreur : le champ "Nom" est obligatoire');
			return;
		}

		fetch('http://localhost:8080/api/v1/pizzas', {
			method: 'POST',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify(pizza),
		}).then(response => {
			if (response.ok) {
				alert(`La pizza ${pizza.name} a été ajoutée !`);
				Router.navigate('/');
			} else {
				alert(`Il y a une erreur pendant la création de la pizza`);
			}
		});
	}

	initPizza(input) {
		return this.element.querySelector(`input[name="${input}"]`).value;
	}
}
